package com.hgh806.galaxy.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.hgh806.galaxy.R
import com.hgh806.galaxy.adapters.FilterType
import com.hgh806.galaxy.adapters.SortAdapter
import kotlinx.android.synthetic.main.content_main.*
import java.util.HashMap

private const val ARG_PARAM = "param"

class SortFragment : Fragment() {
    private var sorts : ArrayList<String> = ArrayList()
    private var param: HashMap<String, String>? = null
    private var sortBy: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param = it.getSerializable(ARG_PARAM) as HashMap<String, String>?
        }

        sortBy = param?.get("sort_by") // get selected sort item
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sort, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
    }

    private fun initRecyclerView() {
        val sortAdapter = SortAdapter()

        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = sortAdapter
        }

        sortAdapter.addData(createList(), sorts.indexOf(sortBy), FilterType.SORT_BY)
    }

    private fun createList(): ArrayList<String>{
        sorts.add("title")
        sorts.add("year")
        sorts.add("rating")
        sorts.add("peers")
        sorts.add("seeds")
        sorts.add("downloads")
        sorts.add("likes")

        return sorts
    }


    companion object {

        @JvmStatic
        fun newInstance(param: HashMap<String, String>) =
            SortFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM, param)
                }
            }
    }
}