package com.hgh806.galaxy

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hgh806.galaxy.models.Torrents
import kotlinx.android.synthetic.main.fragment_torrent_info.*


class TorrentInfoFragment : Fragment() {
    private var torrents: Torrents? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { torrents = it.getParcelable("detail") }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_torrent_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtQuality.text = torrents?.quality
        txtSize.text = torrents?.size
        txtPeers.text = torrents?.peers.toString()
        txtSeeds.text = torrents?.seeds.toString()

        btnDownload.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.addCategory(Intent.CATEGORY_DEFAULT)
            intent.setDataAndType(Uri.parse(torrents?.url) ,"text/plain")
            val chooserIntent = Intent.createChooser(intent, "Open in: ")
            startActivity(chooserIntent)

        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param: Torrents) =
            TorrentInfoFragment().apply {
                arguments = Bundle().apply {
                    putParcelable("detail", param)
                }
            }
    }
}
