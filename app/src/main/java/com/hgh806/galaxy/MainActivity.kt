package com.hgh806.galaxy

import android.animation.ValueAnimator
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.chip.Chip
import com.google.android.material.navigation.NavigationView
import com.hgh806.galaxy.adapters.MovieListAdapter
import com.hgh806.galaxy.models.MovieModel
import com.hgh806.galaxy.models.SearchParamModel
import com.hgh806.galaxy.search.FilterFragment
import com.hgh806.galaxy.service.ApiFactory
import com.hgh806.galaxy.service.ApiService
import com.hgh806.galaxy.utils.Animators
import com.hgh806.galaxy.utils.PaginationHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() , NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener{
    private lateinit var toggle: ActionBarDrawerToggle

    var isLastPage: Boolean = false
    var isLoading: Boolean = false
    private var page: Int = 1
    private var limit: Int = 15
    private val paramsMap: HashMap<String, String> = HashMap()


    private lateinit var progressAnimator: ValueAnimator
    private lateinit var movieListAdapter: MovieListAdapter

    private var apiService: ApiService = ApiFactory.apiService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        toolbar.setTitleTextColor(resources.getColor(R.color.cod_gray))


        toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        progressAnimator =  Animators.makeDeterminateCircularPrimaryProgressAnimator(progressBar)
        startProgress()

        initRecyclerView()

        getData()

        navigationView.setNavigationItemSelectedListener(this)

        refreshLayout.setOnRefreshListener(this)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        toggle.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        val searchViewItem = menu.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(searchViewItem) as SearchView
        val txtSearch: EditText = searchView.findViewById<View>(androidx.appcompat.R.id.search_src_text) as EditText

        setSearchIcons(searchView)

        txtSearch.hint = resources.getString(R.string.search)
        txtSearch.setHintTextColor(Color.LTGRAY)
        txtSearch.setTextColor(resources.getColor(R.color.cod_gray))

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                onMessageEvent(SearchParamModel("query_term", query.trim()))
                searchByFilters()
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    private fun setSearchIcons(searchView: SearchView) {
        try {
            var searchField = SearchView::class.java.getDeclaredField("mCloseButton")
            searchField.isAccessible = true
            val closeBtn =
                searchField.get(searchView) as ImageView
            closeBtn.setImageResource(R.drawable.ic_baseline_close_24)
            searchField =
                SearchView::class.java.getDeclaredField("mVoiceButton")
            searchField.isAccessible = true
            val voiceBtn =
                searchField.get(searchView) as ImageView
            voiceBtn.setImageResource(R.drawable.ic_correct)

        } catch (e: NoSuchFieldException) {
            Log.e("SearchView", e.message, e)
        } catch (e: IllegalAccessException) {
            Log.e("SearchView", e.message, e)
        }
    }

    private fun getData() {
        page = 1
        limit = 15

        val paramsMap: MutableMap<String, String> = HashMap()
        paramsMap["page"] = page.toString()
        paramsMap["limit"] = limit.toString()

        val call: Call<MovieModel> = apiService.getListMovie(paramsMap)
        call.enqueue(object: Callback<MovieModel>{
            override fun onResponse(call: Call<MovieModel>, response: Response<MovieModel>) {
                Log.i("mainActivity","response: + ${response.body()}")
                val movieModel: MovieModel ?= response.body()
                movieModel?.data?.movies?.let { movieListAdapter.submitList(it) }

                endProgress()
            }

            override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                Toast.makeText(this@MainActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                Log.i("mainActivity","getListMovie onFailure: + $t")

                endProgress()
            }

        })
    }

    private fun initRecyclerView() {
        recyclerView.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            movieListAdapter = MovieListAdapter()
            adapter = movieListAdapter

            recyclerView?.addOnScrollListener(object : PaginationHelper(layoutManager as GridLayoutManager) {
                override fun isLastPage(): Boolean {
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    return isLoading
                }

                override fun loadMoreItems() {
                    isLoading = true
                    startProgress()

                    //you have to call loadmore items to get more data
                    getMoreData()
                }
            })
        }
    }

    private fun getMoreData() {
        paramsMap["page"] = page.toString()
        paramsMap["limit"] = limit.toString()

        val call: Call<MovieModel> = apiService.getListMovie(paramsMap)
        call.enqueue(object: Callback<MovieModel>{
            override fun onResponse(call: Call<MovieModel>, response: Response<MovieModel>) {
                Log.i("mainActivity","response: + ${response.body()}")
                val movieModel: MovieModel ?= response.body()
                movieModel?.data?.movies?.let { movieListAdapter.addData(it) }

                endProgress()
            }

            override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                Toast.makeText(this@MainActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                Log.i("mainActivity","error: + $t")

                endProgress()
            }

        })

        getMoreItems()
    }

    private fun getMoreItems() {
        page++
        isLoading = false
    }

    private fun startProgress(){
        progressAnimator.start()
        progressBar.visibility = View.VISIBLE
    }

    private fun endProgress(){
        refreshLayout.isRefreshing = false
        progressAnimator.end()
        progressBar.visibility = View.GONE
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_filter -> {
                for (fragment in supportFragmentManager.fragments) {
                    supportFragmentManager.beginTransaction().remove(fragment).commit()
                }
                replaceFragment(FilterFragment.newInstance(paramsMap))
            }
            R.id.nav_contact -> {
                replaceFragment(SupportFragment.newInstance())
            }

        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun AppCompatActivity.replaceFragment(fragment: Fragment){
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.mainFrame,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(model: SearchParamModel) {
        paramsMap[model.key] = model.param
        removeChip(model)
        addChip(model)
    }

    private fun removeChip(model: SearchParamModel) {
        chip_group.removeView(chip_group.findViewWithTag(model.key))
    }


    private fun addChip(model: SearchParamModel) {
        val chip = Chip(this)
        val paddingDp = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 1f, resources.displayMetrics
        ).toInt()
        chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp)
        chip.text = model.param
        chip.tag = model.key
        chip.setCloseIconResource(R.drawable.ic_baseline_close_24)
        chip.isCloseIconEnabled = true
        chip_group.addView(chip)

        chip.setOnCloseIconClickListener {
            chip_group.removeView(chip)
            paramsMap.remove(model.key)
        }
    }


    fun searchByFilters() {
        page = 1
        movieListAdapter.clearData()
        getMoreData()
    }


    override fun onRefresh() {
        getMoreData()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


}
