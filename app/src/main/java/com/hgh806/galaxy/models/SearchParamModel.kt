package com.hgh806.galaxy.models

import androidx.annotation.Keep

@Keep
data class SearchParamModel(val key:String, val param:String)