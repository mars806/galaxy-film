package com.hgh806.galaxy.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class Data (

	@SerializedName("movie_count") val movie_count : Int,
	@SerializedName("limit") val limit : Int,
	@SerializedName("page_number") val page_number : Int,
	@SerializedName("movies") val movies : List<Movies>
)