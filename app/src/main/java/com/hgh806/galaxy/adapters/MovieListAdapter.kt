package com.hgh806.galaxy.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hgh806.galaxy.MovieDetailsActivity
import com.hgh806.galaxy.R
import com.hgh806.galaxy.models.Movies
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.movie_list_item.view.*

class MovieListAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items : ArrayList<Movies> = ArrayList()
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        context = parent.context

        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_list_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is MyViewHolder -> {
                holder.bind(items[position])
            }
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(context, MovieDetailsActivity::class.java)
            intent.putExtra("detail", items[position])
            context?.startActivity(intent)

        }
    }


    fun submitList(torrentList: List<Movies>){
        items = torrentList as ArrayList<Movies>
        notifyDataSetChanged()
    }

    fun addData(torrentList: List<Movies>) {
        val size = this.items.size
        this.items.addAll(torrentList)
        val sizeNew = this.items.size
        notifyItemRangeChanged(size, sizeNew)
    }

    fun clearData(){
        val size = items.size
        items.clear()
        notifyItemRangeRemoved(0, size)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtTitle = itemView.txtName
        private val imgCover = itemView.imgMovie

        fun bind(movieModel: Movies){

            txtTitle.text = movieModel.title_long
            val picasso:Picasso = Picasso.get()
            picasso.load(movieModel.medium_cover_image).into(imgCover)
        }

    }
}