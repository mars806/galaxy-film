package com.hgh806.galaxy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.gigamole.navigationtabstrip.NavigationTabStrip
import com.hgh806.galaxy.models.Movies
import kotlinx.android.synthetic.main.app_bar.*


class DetailsFragment : Fragment(){

    var movie : Movies? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance() = DetailsFragment()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let { getData(it) }
    }

    private fun getData(bundle: Bundle) {
        movie = bundle.getParcelable("detail")
        if (movie == null)
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
        else {
            val adapter = movie?.let { PlansPagerAdapter(activity?.supportFragmentManager, it) }
            viewPager.adapter = adapter
            viewPager.offscreenPageLimit = 1

            setupTabs()
        }
    }

    private fun setupTabs() {
        navTabScript.setTitles(movie?.torrents?.get(0)?.quality, movie?.torrents?.get(1)?.quality)
        navTabScript.stripType = NavigationTabStrip.StripType.LINE
        navTabScript.stripGravity = NavigationTabStrip.StripGravity.BOTTOM
        navTabScript.setViewPager(viewPager)

    }

    class PlansPagerAdapter(fm: FragmentManager?, var movies: Movies) : FragmentStatePagerAdapter(fm!!) {
        override fun getItem(position: Int): Fragment {
            return TorrentInfoFragment.newInstance(movies.torrents[position])
        }

        override fun getCount(): Int {
            return movies.torrents.size
        }

    }
}