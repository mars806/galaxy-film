package com.hgh806.galaxy

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.hgh806.galaxy.models.Movies
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_movie_details.*
import kotlinx.android.synthetic.main.movie_list_item.txtName

class MovieDetailsActivity : AppCompatActivity() {

    var movie : Movies? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)

        getData()

        btnTorrent.setOnClickListener {
            replaceFragment(DetailsFragment.newInstance())
        }

    }

    private fun AppCompatActivity.replaceFragment(fragment: Fragment){
        val bundle = Bundle()
        bundle.putParcelable("detail", movie)
        fragment.arguments = bundle
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun getData() {
        movie = intent.getParcelableExtra("detail")
        if (movie == null)
            finish()
        else
            setView()
    }

    private fun setView() {
        txtName.text = movie?.title
        txtGenre.text = movie?.genres.toString()
        txtRate.text = movie?.rating.toString()
        txtLanguage.text = movie?.language
        txtSummary.text = movie?.summary
        txtYear.text = movie?.year.toString()

        val picasso: Picasso = Picasso.get()
        picasso.load(movie?.large_cover_image).into(imgCover)

    }


}
