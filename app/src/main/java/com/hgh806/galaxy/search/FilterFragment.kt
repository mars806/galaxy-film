package com.hgh806.galaxy.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.hgh806.galaxy.MainActivity
import com.hgh806.galaxy.R
import kotlinx.android.synthetic.main.fragment_filter.*
import java.util.*
import kotlin.collections.ArrayList

private const val NUM_PAGES = 3
private const val ARG_PARAM = "param"

class FilterFragment : Fragment() {

    private var param: HashMap<String, String> = HashMap()

    companion object {

        @JvmStatic
        fun newInstance(param: HashMap<String, String>) =
            FilterFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM, param)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param = (it.getSerializable(ARG_PARAM) as HashMap<String, String>?)!!
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = activity?.supportFragmentManager?.let { PagerAdapter(it) }
        pagerAdapter?.addFragment(RatingFragment.newInstance(param))
        pagerAdapter?.addFragment(GenreFragment.newInstance(param))
        pagerAdapter?.addFragment(SortFragment.newInstance(param))
        filterPager.offscreenPageLimit = 2
        filterPager.adapter = pagerAdapter


        txtMinRate.setOnClickListener {
            filterPager.currentItem = 0
            changeBackgroundColor(R.color.trial, it)

        }

        txtGenre.setOnClickListener {
            filterPager.currentItem = 1
            changeBackgroundColor(R.color.revolver, it)
        }

        txtSort.setOnClickListener {
            filterPager.currentItem = 2
            changeBackgroundColor(R.color.swamp, it)

        }

        btnSearch.setOnClickListener{
            (activity as MainActivity?)?.searchByFilters()
            activity?.onBackPressed()
        }
    }

    private fun changeBackgroundColor(colorId: Int, view: View){
        txtSort.setBackgroundColor(resources.getColor(R.color.cod_gray))
        txtGenre.setBackgroundColor(resources.getColor(R.color.cod_gray))
        txtMinRate.setBackgroundColor(resources.getColor(R.color.cod_gray))

        view.setBackgroundColor(resources.getColor(colorId))
    }

    private inner class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val mList: MutableList<Fragment> = ArrayList()

        override fun getCount(): Int = NUM_PAGES

        override fun getItem(position: Int): Fragment{
            return mList[position]
        }
        fun addFragment(fragment: Fragment) {
            mList.add(fragment)
        }
    }
}