package com.hgh806.galaxy.service

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import hgh.mars.gettorrent.utils.AppConstants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiFactory {
    //Creating Auth Interceptor to add api_key query in front of all the requests.
    private val authInterceptor = Interceptor { chain ->
        val newUrl = chain.request()
            .newBuilder()
            .addHeader("x-rapidapi-key", AppConstants.API_KEY)
            .addHeader("x-rapidapi-host", AppConstants.x_rapidapi_host)
            //.addQueryParameter("content-type",AppConstants.content_type)
            .build()

        chain.proceed(newUrl)
    }

    //OkhttpClient for building http request url
    private val tmdbClient = OkHttpClient().newBuilder()
        .addInterceptor(authInterceptor)
        .build()


    private fun retrofit(): Retrofit = Retrofit.Builder()
        .client(tmdbClient)
        .baseUrl("https://yts-am-torrent.p.rapidapi.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()


    val apiService: ApiService = retrofit().create(ApiService::class.java)

}