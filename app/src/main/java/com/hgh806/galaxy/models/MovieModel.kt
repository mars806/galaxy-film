package com.hgh806.galaxy.models

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class MovieModel (

	@SerializedName("status") val status : String,
	@SerializedName("status_message") val status_message : String,
	@SerializedName("data") val data : Data
)