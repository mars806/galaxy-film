package com.hgh806.galaxy.models

import androidx.annotation.Keep

@Keep
class SimpleStringModel(val text: String)