package com.hgh806.galaxy.service

import com.hgh806.galaxy.models.MovieModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface ApiService {

    @GET("list_movies.json")
    fun getListMovie(@QueryMap mapParams: Map<String ,String>): Call<MovieModel>

    @GET("list_movies.json")
    fun getMovieDetails(@Query("movie_id") movie_id:Int ): Call<MovieModel>
}