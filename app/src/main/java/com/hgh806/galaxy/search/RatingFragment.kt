package com.hgh806.galaxy.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.hgh806.galaxy.R
import com.hgh806.galaxy.models.SearchParamModel
import kotlinx.android.synthetic.main.fragment_rating.*
import org.greenrobot.eventbus.EventBus
import java.util.HashMap

private const val ARG_PARAM = "param"

class RatingFragment : Fragment() {
    private var param: HashMap<String, String>? = null
    private var rate: String = "6"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param = it.getSerializable(ARG_PARAM) as HashMap<String, String>?
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        if(param?.get("minimum_rating") != null) {
            rate = param!!["minimum_rating"].toString()
            ratingBar.rating = rate.toFloat()
            txtRate.text = rate
        }else
            setRate(6f)

        ratingBar.setOnRatingBarChangeListener { _, fl, _ ->
            setRate(fl)
        }
    }

    private fun setRate(rate: Float) {
        if (rate >= 1) {
            txtRate.text = rate.toString()
            EventBus.getDefault().post(SearchParamModel("minimum_rating", rate.toString()))
        }else{
            txtRate.text = "1"
            ratingBar.rating = 1F
            EventBus.getDefault().post(SearchParamModel("minimum_rating", "1"))

        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param: HashMap<String, String>) =
            RatingFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM, param)
                }
            }
    }

}