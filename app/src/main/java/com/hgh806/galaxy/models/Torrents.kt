package com.hgh806.galaxy.models

import android.os.Parcelable
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Torrents (

	@SerializedName("url") val url : String,
	@SerializedName("hash") val hash : String,
	@SerializedName("quality") val quality : String,
	@SerializedName("type") val type : String,
	@SerializedName("seeds") val seeds : Int,
	@SerializedName("peers") val peers : Int,
	@SerializedName("size") val size : String,
	@SerializedName("size_bytes") val size_bytes : Long,
	@SerializedName("date_uploaded") val date_uploaded : String,
	@SerializedName("date_uploaded_unix") val date_uploaded_unix : Long
): Parcelable