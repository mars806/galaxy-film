package com.hgh806.galaxy.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.hgh806.galaxy.R
import com.hgh806.galaxy.adapters.FilterType
import com.hgh806.galaxy.adapters.SortAdapter
import kotlinx.android.synthetic.main.content_main.*
import java.util.HashMap

private const val ARG_PARAM = "param"

class GenreFragment : Fragment() {
    private var genres : ArrayList<String> = ArrayList()
    private var param: HashMap<String, String>? = null
    private var genre: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param = it.getSerializable(ARG_PARAM) as HashMap<String, String>?
        }

        genre = param?.get("genre") // get selected genre
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_genre, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
    }

    private fun initRecyclerView() {
        val sortAdapter = SortAdapter()

        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = sortAdapter
        }

        sortAdapter.addData(createList(), genres.indexOf(genre), FilterType.GENRE)
    }

    private fun createList(): ArrayList<String>{
        genres.add("Action")
        genres.add("Adventure")
        genres.add("Animation")
        genres.add("Biography")
        genres.add("Comedy")
        genres.add("Crime")
        genres.add("Documentary")
        genres.add("Drama")
        genres.add("Family")
        genres.add("Fantasy")
        genres.add("Film Noir")
        genres.add("History")
        genres.add("Horror")
        genres.add("Music")
        genres.add("Musical")
        genres.add("Mystery")
        genres.add("Sci-Fi")
        genres.add("Romance")
        genres.add("Short")
        genres.add("Superhero")
        genres.add("Thriller")
        genres.add("War")
        genres.add("Western")

        return genres
    }


    companion object {

        @JvmStatic
        fun newInstance(param: HashMap<String, String>) =
            GenreFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM, param)
                }
            }
    }

}