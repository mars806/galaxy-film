package com.hgh806.galaxy.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hgh806.galaxy.R
import com.hgh806.galaxy.models.SearchParamModel
import kotlinx.android.synthetic.main.recycler_simple_item.view.*
import org.greenrobot.eventbus.EventBus
import java.util.*
import kotlin.collections.ArrayList

enum class FilterType(val backgroundColor: Int, val selectedColor: Int) {
    SORT_BY(R.color.swamp, R.color.aqua_deep)
    , GENRE(R.color.revolver ,R.color.ripe_plum )

}

class SortAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var rowIndex: Int = -1
    private var items : ArrayList<String> = ArrayList()
    private var context: Context? = null
    private var type: FilterType = FilterType.GENRE
    private var key: String = "genre"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context

        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_simple_item, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is MyViewHolder -> {
                holder.bind(items[position])
            }
        }

        holder.itemView.setOnClickListener {
            rowIndex = position
            EventBus.getDefault().post(SearchParamModel( key, items[position]))
            notifyDataSetChanged()
        }

        if(rowIndex==position){
            context?.resources?.getColor(type.selectedColor)?.let {
                holder.itemView.sortTextView.setBackgroundColor(
                    it
                )
            }
        } else {
            context?.resources?.getColor(type.backgroundColor)?.let {
                holder.itemView.sortTextView.setBackgroundColor(
                    it
                )
            }
        }
    }

    fun addData(model: List<String>, selectedIndex: Int, type: FilterType) {
        key = type.name.toLowerCase(Locale.ROOT)
        this.type = type

        rowIndex = selectedIndex
        val size = this.items.size
        this.items.addAll(model)
        val sizeNew = this.items.size
        notifyItemRangeChanged(size, sizeNew)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val sortTextView = itemView.sortTextView

        fun bind(text: String){
            sortTextView.text = text
        }

    }
}